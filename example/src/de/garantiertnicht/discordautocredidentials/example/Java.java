/*
 * DiscordAutoCredidentials, a programm that looks up the discord tokens from local clients.
 * Copyright (C) 2017 garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.discordautocredidentials.example;

import de.garantiertnicht.DiscordAutoCredidentials.discord.ClientToken;
import de.garantiertnicht.DiscordAutoCredidentials.discord.DiscordClientRegistry;
import de.garantiertnicht.DiscordAutoCredidentials.discord.DiscordClientRegistry$;
import scala.collection.JavaConverters;
import scala.collection.Seq;

import java.util.List;

/**
 * Prints all tokens
 */
public class Java {
    public static void main(String... args) {
        DiscordClientRegistry clientRegistry = DiscordClientRegistry$.MODULE$;
        Seq<ClientToken> tokensSeq = clientRegistry.clientTokens(clientRegistry.clientTokens$default$1());
        List<ClientToken> tokens = JavaConverters.seqAsJavaList(tokensSeq);

        tokens.forEach((token) -> {
            System.out.println(String.format("%s: %s", token.discordClient(), token));
        });
    }
}
