/*
 * DiscordAutoCredidentials, a programm that looks up the discord tokens from local clients.
 * Copyright (C) 2017 garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.DiscordAutoCredidentials.utils

/**
  * Represents a holder of multiple objects of one type.
  * @param values '''Non-Empty''' list of values
  * @tparam T The type to store
  * @throws AssertionError If the list is empty
  */
abstract class Registry[T <: Ordered[T]](values: T*) {
  assert(values.nonEmpty)

  /**
    * Ordered version of the values
    */
  val allKnown: Seq[T] = values.sorted
}