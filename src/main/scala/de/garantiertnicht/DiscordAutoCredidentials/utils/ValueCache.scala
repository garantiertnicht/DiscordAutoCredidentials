/*
 * DiscordAutoCredidentials, a programm that looks up the discord tokens from local clients.
 * Copyright (C) 2017 garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.DiscordAutoCredidentials.utils

import scala.collection.mutable

/**
  * Stores a input-result pair for a function.
  * @param function The function to evaluate if there is no stored result
  * @tparam T The type of the result
  * @tparam I The type of the output
  */
class ValueCache[T, I](function: (I) => T) {
  /**
    * Stores all known input-result-pairs
    */
  private var values = new mutable.HashMap[I, T]

  /**
    * Gets the result for the given input. This will either look up the cached result or apply the function.
    * @param input The input for the function
    * @return The result
    */
  def apply(input: I): T = {
    if(!values.contains(input)) {
      val result = function(input)
      values += ((input, result))

      result
    } else {
      values(input)
    }
  }
}
