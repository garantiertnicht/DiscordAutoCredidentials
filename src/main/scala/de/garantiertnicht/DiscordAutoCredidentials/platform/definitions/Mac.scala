/*
 * DiscordAutoCredidentials, a programm that looks up the discord tokens from local clients.
 * Copyright (C) 2017 garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.DiscordAutoCredidentials.platform.definitions

import java.util.Locale

import de.garantiertnicht.DiscordAutoCredidentials.platform.Platform

object Mac extends Platform {

  /**
    * A human-friendly name of the da.garantiertnicht.DiscordAutoCredidentials.platform
    */
  override val name: String = "macOS"

  /**
    * Gets the user config directory for the given system.
    *
    * @return The system-dependent config directory ending with the file separator
    */
  override def configDirectory: String = {
    val home = sys.props("user.home")

    if(home.endsWith("/")) {
      return home + "Library/Application Support/"
    }

    home + "/Library/Application Support/"
  }

  /**
    * Returns if the current [[de.garantiertnicht.DiscordAutoCredidentials.platform]] could apply.
    *
    * @return True, if this [[de.garantiertnicht.DiscordAutoCredidentials.platform]] can get the right directory
    */
  override def applies: Boolean = {
    val os = sys.props("os.name").toLowerCase(Locale.ENGLISH)

    os.contains("mac") || os.contains("darwin")
  }

  /**
    * Gets the order in which applies gets called
    */
  override val sortOrder: Byte = 0
}
