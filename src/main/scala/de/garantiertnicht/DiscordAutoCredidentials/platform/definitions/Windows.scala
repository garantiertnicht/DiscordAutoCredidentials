/*
 * DiscordAutoCredidentials, a programm that looks up the discord tokens from local clients.
 * Copyright (C) 2017 garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.DiscordAutoCredidentials.platform.definitions

import java.io.File
import java.util.Locale

import de.garantiertnicht.DiscordAutoCredidentials.platform.Platform

object Windows extends Platform {

  /**
    * A human-friendly name of the da.garantiertnicht.DiscordAutoCredidentials.platform
    */
  override val name: String = "Microsoft Windows"

  /**
    * Gets the user config directory for the given system.
    *
    * @return The system-dependent config directory ending with the file separator
    */
  override def configDirectory: String = {
    val path = sys.env("APPDATA")

    if(!path.endsWith(File.pathSeparator) && !path.endsWith("/")) {
      return path + "/"
    }

    path
  }

  /**
    * Returns if the current [[de.garantiertnicht.DiscordAutoCredidentials.platform]] could apply.
    *
    * @return True, if this [[de.garantiertnicht.DiscordAutoCredidentials.platform]] can get the right directory
    */
  override def applies: Boolean = sys.props("os.name").toLowerCase(Locale.ENGLISH).startsWith("win")

  /**
    * Gets the order in which applies gets called
    */
  override val sortOrder: Byte = 1
}
