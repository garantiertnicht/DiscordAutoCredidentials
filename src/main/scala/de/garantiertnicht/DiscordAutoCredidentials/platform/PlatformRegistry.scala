/*
 * DiscordAutoCredidentials, a programm that looks up the discord tokens from local clients.
 * Copyright (C) 2017 garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.DiscordAutoCredidentials.platform

import de.garantiertnicht.DiscordAutoCredidentials.platform.definitions.{Mac, Windows, XdgCompatible}
import de.garantiertnicht.DiscordAutoCredidentials.utils.Registry

/**
  * Lists all known platforms.
  */
class PlatformRegistry(default: Platform, other: Platform*) extends Registry[Platform](other.toBuffer += default: _*) {
  /**
    * Returns the current platform
    */
  val currentPlatform: Platform = allKnown.find(_.applies).getOrElse(default)
}

/**
  * Default [[de.garantiertnicht.DiscordAutoCredidentials.platform.PlatformRegistry]] with all Clients
  */
object PlatformRegistry extends PlatformRegistry(XdgCompatible, Mac, Windows)