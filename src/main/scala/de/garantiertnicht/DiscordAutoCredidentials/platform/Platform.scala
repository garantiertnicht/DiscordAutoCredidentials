/*
 * DiscordAutoCredidentials, a programm that looks up the discord tokens from local clients.
 * Copyright (C) 2017 garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.DiscordAutoCredidentials.platform

/**
  * Represents a platform, typically an operating system.
  * Used to get the path of the config-files
  */
abstract class Platform extends Ordered[Platform] {
  //---- GENERIC SETTINGS -----//
  /**
    * A human-friendly name of the [[de.garantiertnicht.DiscordAutoCredidentials.platform.Platform]]
    */
  val name: String

  //---- DETECT CURRENT OS ----//
  /**
    * Gets the user config directory for the given system.
    *
    * @return The system-dependent config directory ending with the file separator
    */
  def configDirectory: String

  /**
    * Returns if the current [[de.garantiertnicht.DiscordAutoCredidentials.platform]] could apply.
    *
    * @return True, if this [[de.garantiertnicht.DiscordAutoCredidentials.platform]] can get the right directory
    */
  def applies: Boolean

  /**
    * Gets the order in which applies gets called
    */
  val sortOrder: Byte

  //---- QOL METHODS ---------//

  /**
    * Gets the [[de.garantiertnicht.DiscordAutoCredidentials.platform.Platform#name]] of the operating system
    * @return [[de.garantiertnicht.DiscordAutoCredidentials.platform.Platform#name]]
    */
  override def toString: String = name

  override def compare(that: Platform): Int = sortOrder.compare(that.sortOrder)
}
