/*
 * DiscordAutoCredidentials, a programm that looks up the discord tokens from local clients.
 * Copyright (C) 2017 garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.DiscordAutoCredidentials.discord

import de.garantiertnicht.DiscordAutoCredidentials.platform.Platform
import de.garantiertnicht.DiscordAutoCredidentials.utils.ValueCache

/**
  * Represents a Discord Client which stores the token.
  */
abstract class DiscordClient extends Ordered[DiscordClient] {
  //---- GENERIC SETTINGS -----//

  /**
    * A user-friendly name for the discord client
    */
  val name: String

  /**
    * The name inside the platform's config directory where the client stores its data.
    * Must be without slash in font, but with a slash behind.
    *
    * @example discordcanary/
    */
  val configFileOrDirectoryName: String

  /**
    * The URL which that DiscordClient uses
    * @note This is not necessarily the download-url for the client, but the API URL it uses
    * @example canary.discordapp.com
    */
  val accessingUrl: String

  /**
    * A sorting order for the client. Should be lower if this client is more widespread.
    */
  val sortingOrder: Byte

  /**
    * A file containing the client token. Used to determinate if the client is installed
    */
  val tokenFilePath: ValueCache[String, Platform]

  /**
    * Gets an optional Client-Token pair
    */
  val token: ValueCache[Option[String], Platform]

  //---- QOL METHODS ---------//

  /**
    * Gets the [[de.garantiertnicht.DiscordAutoCredidentials.discord.DiscordClient#name]]
    * @return [[de.garantiertnicht.DiscordAutoCredidentials.discord.DiscordClient#name]]
    */
  override def toString: String = name

  override def compare(that: DiscordClient): Int = sortingOrder.compareTo(that.sortingOrder)
}
