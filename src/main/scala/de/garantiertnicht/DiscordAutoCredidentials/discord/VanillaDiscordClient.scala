package de.garantiertnicht.DiscordAutoCredidentials.discord

import java.sql.DriverManager
import java.util.Properties

import de.garantiertnicht.DiscordAutoCredidentials.platform.Platform
import de.garantiertnicht.DiscordAutoCredidentials.utils.ValueCache
import org.sqlite.SQLiteConfig

/**
  * Implements methods for all Clients that store the token the same as vanilla does.
  * This will change if the vanilla clients will change the way tokens are stored!
  */
abstract class VanillaDiscordClient extends DiscordClient {
  /**
    * This should return the absolute path to the file which stores the client token.
    */
  val tokenFilePath = new ValueCache[String, Platform](platform => {
    val path = new StringBuilder()
    path.append(platform.configDirectory)
    path.append(configFileOrDirectoryName)
    path.append("Local Storage/https_")
    path.append(accessingUrl)
    path.append("_0.localstorage")

    path.mkString
  })

  /**
    * Gets a setting for JDBC where SQLite will open the database read-only
    */
  private final val sqliteReadOnly: Properties = {
    val settings = new SQLiteConfig
    settings.setReadOnly(true)
    settings.toProperties
  }

  /**
    * Gets an optional Client-Token pair
    */
  val token = new ValueCache[Option[String], Platform](platform => {
    try {
      val connection = DriverManager.getConnection("jdbc:sqlite:" + tokenFilePath(platform), sqliteReadOnly)
      val statement = connection.createStatement()
      statement.setQueryTimeout(2)

      val resultSet = statement.executeQuery("SELECT value FROM ItemTable WHERE key = 'token'")

      if (resultSet.next()) {
        val token = resultSet.getString("value")
        connection.close()
        Some(token.substring(1, token.length - 1))
      } else {
        connection.close()
        None
      }
    } catch {
      case exception: Exception => {
        None
      }
    }
  })
}
