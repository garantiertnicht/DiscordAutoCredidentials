/*
 * DiscordAutoCredidentials, a programm that looks up the discord tokens from local clients.
 * Copyright (C) 2017 garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.DiscordAutoCredidentials.discord

import java.nio.file.{FileSystems, Files}

import de.garantiertnicht.DiscordAutoCredidentials.discord.definitions.{Discord, DiscordCanary, DiscordPublicTestingBuild}
import de.garantiertnicht.DiscordAutoCredidentials.platform.{Platform, PlatformRegistry}
import de.garantiertnicht.DiscordAutoCredidentials.utils.{Registry, ValueCache}

/**
  * List all known [[de.garantiertnicht.DiscordAutoCredidentials.discord.DiscordClient]]s and has the capabilities
  * to list all installed platforms and get all tokens available.
  */
class DiscordClientRegistry(clients: DiscordClient*) extends Registry[DiscordClient](clients: _*) {
  /**
    * Checks for all [[de.garantiertnicht.DiscordAutoCredidentials.discord.DiscordClientRegistry#knownDiscordClients()]] where the
    * [[de.garantiertnicht.DiscordAutoCredidentials.discord.DiscordClient#tokenFilePath]] exists.
    * @example installedClients(Windows)
    * @note For the current platorm, use [[de.garantiertnicht.DiscordAutoCredidentials.discord.DiscordClientRegistry#installedClientsForCurrentPlatform()]]
    */
  val installedClients = new ValueCache[Seq[DiscordClient], Platform]((platform) =>
    allKnown.filter(client => Files.exists(FileSystems.getDefault.getPath(client.tokenFilePath(platform))))
  )

  /**
    * Lists all [[de.garantiertnicht.DiscordAutoCredidentials.discord.DiscordClient]]s for the current platform.
    * @return A list of [[de.garantiertnicht.DiscordAutoCredidentials.discord.DiscordClient]]s that should be existing
    */
  def installedClientsForCurrentPlatform: Seq[DiscordClient] = {
    installedClients(PlatformRegistry.currentPlatform)
  }

  /**
    * Asks all [[de.garantiertnicht.DiscordAutoCredidentials.discord.DiscordClientRegistry#installedClientsForCurrentPlatform]] for their
    * [[de.garantiertnicht.DiscordAutoCredidentials.discord.DiscordClient#token]]
    * for a specified [[de.garantiertnicht.DiscordAutoCredidentials.platform.Platform]]
    * @param platform The platform to look for. Defaults to
    *                 [[de.garantiertnicht.DiscordAutoCredidentials.platform.PlatformRegistry#currentPlatform]]
    * @return All tokens found
    */
  def clientTokens(platform: Platform = PlatformRegistry.currentPlatform): Seq[ClientToken] = {
    installedClients(platform).filter(_.token(platform).nonEmpty).map(client => ClientToken(client, client.token(platform).get))
  }
}

/**
  * Default [[de.garantiertnicht.DiscordAutoCredidentials.discord.DiscordClientRegistry]] with all Discord Clients
  */
object DiscordClientRegistry extends DiscordClientRegistry(Discord, DiscordPublicTestingBuild, DiscordCanary)