/*
 * DiscordAutoCredidentials, a programm that looks up the discord tokens from local clients.
 * Copyright (C) 2017 garantiertnicht
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.garantiertnicht.DiscordAutoCredidentials.discord.definitions

import de.garantiertnicht.DiscordAutoCredidentials.discord.{DiscordClient, VanillaDiscordClient}

object DiscordPublicTestingBuild extends VanillaDiscordClient {
  /**
    * A user-friendly name for the discord client
    */
  override val name: String = "Discord PTB"
  /**
    * The name inside the platform's config directory where the client stores its data.
    * Must be without slash in font, but with a slash behind.
    *
    * @example discordcanary/
    */
  override val configFileOrDirectoryName: String = "discordptb/"
  /**
    * The URL which that DiscordClient uses
    *
    * @note This is not necessarily the download-url for the client, but the API URL it uses
    * @example canary.discordapp.com
    */
  override val accessingUrl: String = "ptb.discordapp.com"
  /**
    * A sorting order for the client. Should be lower if this client is more widespread.
    */
  override val sortingOrder: Byte = 0
}
